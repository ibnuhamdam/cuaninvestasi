<?php

/**
 * Created by PhpStorm.
 * Date: 23/03/2019
 * Time: 3:02
 */

class AuthModel extends CI_Model
{
	public function login($email, $pass)
	{
		$user = $this->db->get_where('users', array('email' => $email, 'password' => $pass));
		$login = $this->db->affected_rows();
		if ($login == 1) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function find($email)
	{
		$query = $this->db->get_where('users', array('email' => $email));
		return $query->row();
	}
}
