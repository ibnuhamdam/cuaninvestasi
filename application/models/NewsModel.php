<?php

class NewsModel extends CI_Model
{
	private $_table = "berita";

	private function _uploadImage()
	{
		$config['upload_path']          = './uploads/news/gambar';
		$config['allowed_types']        = 'jpg|gif|png|jpeg';
		$config['file_name']            = "news_" . uniqid();
		$config['overwrite']			= true;
		$config['max_size']             = 1024; // 1MB

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('image')) {
			return $this->upload->data("file_name");
		}
		print_r($this->upload->display_errors());
	}

	private function _deleteImage($id)
	{
		$news = $this->getNews($id);
		$filename = explode(".", $news[0]->gambar)[0];
		return array_map('unlink', glob(FCPATH . "uploads/news/gambar/$filename.*"));
	}

	public function getNews($id = Null)
	{
		if ($id == null) {
			return $this->db->get($this->_table)->result();
		} else {
			return $this->db->get_where($this->_table, array('id' => $id))->result();
		}
	}

	public function getNewsApi($q = null, $page)
	{
		$_apikey = 'c11eb54d5f444e45a497c4164a451402';
		$curl = curl_init();
		if ($q == null) {
			$q = "https://newsapi.org/v2/top-headlines?country=id&pageSize=$page&apiKey=$_apikey";
		} else if ($q == 'Covid') {
			$q = "https://newsapi.org/v2/top-headlines?country=id&pageSize=$page&apiKey=$_apikey&q=covid";
		} else if ($q == 'Sports') {
			$q = "https://newsapi.org/v2/top-headlines?country=id&pageSize=$page&apiKey=$_apikey&category=sports";
		} else if ($q == 'Lainnya') {
			$q = "https://newsapi.org/v2/top-headlines?country=id&pageSize=$page&apiKey=$_apikey";
		} else if ($q == 'health') {
			$q = "https://newsapi.org/v2/top-headlines?country=id&pageSize=$page&apiKey=$_apikey&category=health";
		} else if ($q == 'general') {
			$q = "https://newsapi.org/v2/top-headlines?country=id&pageSize=$page&apiKey=$_apikey&category=general";
		}
		curl_setopt($curl, CURLOPT_URL, $q);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($curl);
		curl_close($curl);

		$result = json_decode($result, true);
		// var_dump($result);

		$articles = $result['articles'];

		return $articles;
	}

	public function save()
	{
		$post = $this->input->post();
		$this->id = '';
		$this->judul = $post["judul"];
		$this->isi = $post["isi"];
		$this->tags = $post["tags"];
		$this->gambar = $this->_uploadImage();
		$this->penulis = $this->session->userdata('nama');
		$this->created_at = date("Y-m-d H:i:s");
		$this->deleted_at = '';
		$insert = $this->db->insert($this->_table, $this);
		if ($insert) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update()
	{
		$post = $this->input->post();
		$this->judul = $post["judul"];
		$this->isi = $post["isi"];
		$this->tags = $post["tags"];

		if (!empty($_FILES["images"]["name"])) {
			$this->gambar = $this->_uploadImage();
		} else {
			$this->gambar = $post["old_image"];
		}

		return $this->db->update($this->_table, $this, array('id' => $post["id"]));
	}

	public function delete($id)
	{
		$this->_deleteImage($id);
		return $this->db->delete($this->_table, array("id" => $id));
	}
}
