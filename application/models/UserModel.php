<?php

class UserModel extends CI_Model
{
	public function update($id, $nama, $email)
	{
		$data = array(
			'nama' => $nama,
			'email' => $email,
			'updated_at' => date("Y-m-d H:i:s")
		);

		$this->db->set($data);
		$this->db->where('id', $id);
		$update = $this->db->update('users');

		if ($update) {
			return true;
		} else {
			return false;
		}
	}
}
