<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Berita extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('NewsModel', 'News');
		if ($this->session->userdata('logged_in') == NULL) {
			redirect('Auth/login');
		}
	}

	public function insert()
	{
		$save =  $this->News->save();

		if ($save) {
			$this->session->set_flashdata('insert', 'success');
			redirect('Admin/home/berita');
		} else {
			$this->session->set_flashdata('insert', 'failed');
			redirect('Admin/home/berita');
		}
	}

	public function delete($id = null)
	{
		$delete = $this->News->delete($id);
		if ($delete) {
			$this->session->set_flashdata('delete', 'success');
			redirect('Admin/home/berita');
		} else {
			$this->session->set_flashdata('delete', 'failed');
			redirect('Admin/home/berita');
		}
	}

	public function update()
	{
		$update = $this->News->update();

		if ($update) {
			$this->session->set_flashdata('update', 'success');
			redirect('Admin/home/berita');
		} else {
			$this->session->set_flashdata('update', 'failed');
			redirect('Admin/home/berita');
		}
	}
}
