<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel', 'User');
		$this->load->model('AuthModel', 'Auth');
		$this->load->model('NewsModel', 'News');
		if ($this->session->userdata('logged_in') == NULL) {
			redirect('Auth/login');
		}
	}

	public function index()
	{
		$data = [
			'title' => 'Admin | CuanInvestasi'
		];
		$this->load->view('layout/admin/header', $data);
		$this->load->view('layout/admin/navbar', $data);
		$this->load->view('admin/index');
		$this->load->view('layout/admin/footer');
	}

	public function berita()
	{
		$berita = $this->News->getNews();
		$data = [
			'title' => 'Admin | Daftar Berita',
			'berita' => $berita
		];
		$this->load->view('layout/admin/header', $data);
		$this->load->view('layout/admin/navbar', $data);
		$this->load->view('admin/dataBerita', $data);
		$this->load->view('layout/admin/footer');
	}

	public function tambahBerita()
	{
		$data = [
			'title' => 'Admin | Tambah Daftar Berita'
		];
		$this->load->view('layout/admin/header', $data);
		$this->load->view('layout/admin/navbar', $data);
		$this->load->view('admin/tambahBerita');
		$this->load->view('layout/admin/footer');
	}

	public function detailBerita($id)
	{
		$berita = $this->News->getNews($id);
		$data = [
			'title' => 'Admin | Detail Berita',
			'berita' => $berita
		];
		$this->load->view('layout/admin/header', $data);
		$this->load->view('layout/admin/navbar', $data);
		$this->load->view('admin/detailBerita', $data);
		$this->load->view('layout/admin/footer');
	}

	public function profile()
	{
		$akun = $this->Auth->find($this->session->userdata('email'));
		$data = [
			'title' => 'Admin | Profile',
			'akun' => $akun
		];
		$this->load->view('layout/admin/header', $data);
		$this->load->view('layout/admin/navbar', $data);
		$this->load->view('admin/profile', $data);
		$this->load->view('layout/admin/footer');
	}

	public function editProfile()
	{
		$akun = $this->Auth->find($this->session->userdata('email'));
		$data = [
			'title' => 'Admin | Edit My Profile',
			'akun' => $akun
		];
		$this->load->view('layout/admin/header', $data);
		$this->load->view('layout/admin/navbar', $data);
		$this->load->view('admin/editProfile', $data);
		$this->load->view('layout/admin/footer');
	}

	public function updateProfile()
	{
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');

		$update =  $this->User->update($id, $nama, $email);

		if ($update) {
			$this->session->set_flashdata('update', 'success');
			redirect('Admin/home/profile');
		} else {
			$this->session->set_flashdata('update', 'failed');
			redirect('Admin/home/profile');
		}
	}

	public function editBerita($id)
	{
		$berita = $this->News->getNews($id);
		$data = [
			'title' => 'Admin | Ubah Berita',
			'berita' => $berita,
			'tinydata' => $berita[0]->isi
		];
		$this->load->view('layout/admin/header', $data);
		$this->load->view('layout/admin/navbar', $data);
		$this->load->view('admin/editBerita', $data);
		$this->load->view('layout/admin/footer', $data);
	}
}
