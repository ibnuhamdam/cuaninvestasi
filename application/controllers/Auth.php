<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		// if ($this->session->userdata('logged_in') == NULL) {
		// 	$this->session->set_flashdata('login', 'belum');
		// } else {
		// 	redirect('Admin/home');
		// }
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') == NULL) {
		} else {
			redirect('Admin/home');
		}
		$data = [
			'title' => 'Home | CuanInvestasi'
		];

		$this->load->view('layout/login/navbar', $data);
		$this->load->view('login/index');
		$this->load->view('layout/login/footer');
	}

	public function login()
	{
		$this->load->library('session');
		$this->load->model('AuthModel', 'Auth');
		$email = $this->input->post('email');
		$pass = $this->input->post('password');


		$akun = $this->Auth->find($email);

		$login = $this->Auth->Login($email, $pass);

		if ($login) {
			$user = array(
				'id' => $akun->id,
				'email'  => $email,
				'nama' => $akun->nama,
				'logged_in' => TRUE
			);

			$this->session->set_userdata($user);

			$this->session->set_flashdata('login', 'success');
			redirect('Admin/home');
		} else {
			$this->session->set_flashdata('login', 'failed');
			redirect('Auth/index');
		}
	}

	public function logout()
	{
		session_destroy();
		redirect('Auth/index');
	}
}
