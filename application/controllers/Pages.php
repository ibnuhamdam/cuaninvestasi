<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pages extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('NewsModel', 'News');
	}

	public function index()
	{

		$beritaApi = $this->News->getNewsApi('', 5);
		$beritaOlahraga = $this->News->getNewsApi('Sports', 2);
		$beritaCovid = $this->News->getNewsApi('Covid', 2);
		$beritaLainnya = $this->News->getNewsApi('Lainnya', 8);

		$berita = $this->News->getNews();
		$data = [
			'title' => 'Home | CuanInvestasi',
			'berita' => $berita,
			'beritaApi' => $beritaApi,
			'beritaOlahraga' => $beritaOlahraga,
			'beritaCovid' => $beritaCovid,
			'beritaLainnya' => $beritaLainnya
		];

		$this->load->view('layout/navbar', $data);
		$this->load->view('home/index', $data);
		$this->load->view('layout/footer');
	}

	public function login()
	{
		$data = [
			'title' => 'Login | CuanInvestasi'
		];
		$this->load->view('layout/navbarAdmin', $data);
		$this->load->view('login/index');
		$this->load->view('layout/footerAdmin');
	}

	public function allnews()
	{
		$beritaApi = $this->News->getNewsApi('', 3);
		$beritaGeneral = $this->News->getNewsApi('general', 8);
		$beritaHealth = $this->News->getNewsApi('health', 6);
		$data = [
			'title' => 'Semua Berita | CuanInvestasi',
			'beritaApi' => $beritaApi,
			'beritaGeneral' => $beritaGeneral,
			'beritaHealth' => $beritaHealth
		];
		$this->load->view('layout/navbar', $data);
		$this->load->view('home/allnews', $data);
		$this->load->view('layout/footer');
	}
}
