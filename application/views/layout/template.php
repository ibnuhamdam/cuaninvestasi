<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,800;1,300;1,400&display=swap" rel="stylesheet">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<!-- Main Css -->
	<link rel="stylesheet" href="/assets/css/style.css">
	<title><?= $title; ?></title>
</head>

<body>
	<?= $this->include('layout/navbar'); ?>

	<?= $this->renderSection('content'); ?>
