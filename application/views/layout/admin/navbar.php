 <!-- Sidenav -->
 <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
 	<div class="scrollbar-inner">
 		<!-- Brand -->
 		<div class="sidenav-header  align-items-center">
 			<a class="navbar-brand" href="javascript:void(0)">
 				<h1 class="text-primary">CuanInvestasi</h1>
 			</a>
 		</div>
 		<div class="navbar-inner">
 			<!-- Collapse -->
 			<div class="collapse navbar-collapse" id="sidenav-collapse-main">
 				<!-- Nav items -->
 				<ul class="navbar-nav">
 					<li class="nav-item">
 						<a class="nav-link active" href="<?= site_url('Admin/home'); ?>">
 							<i class="ni ni-tv-2 text-primary"></i>
 							<span class="nav-link-text">Dashboard</span>
 						</a>
 					</li>
 					<li class="nav-item">
 						<a class="nav-link" href="<?= site_url('Admin/home/profile'); ?>">
 							<i class="ni ni-single-02 text-yellow"></i>
 							<span class="nav-link-text">Profile</span>
 						</a>
 					</li>
 					<li class="nav-item">
 						<a class="nav-link" href="#" id="Berita" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
 							<i class="ni ni-single-copy-04 text-success"></i>
 							Berita
 						</a>
 						<div class="dropdown-menu" aria-labelledby="Berita">
 							<a class="dropdown-item" href="<?= base_url('Admin/home/tambahBerita'); ?>"><i class="ni ni-caps-small text-primary"></i>Tambahkan Berita</a>

 							<a class="dropdown-item" href="<?= base_url('Admin/home/berita'); ?>"> <i class="ni ni-bullet-list-67 text-default"></i>Daftar Berita</a>
 						</div>
 					</li>

 				</ul>
 				<!-- Divider -->
 				<hr class="my-3">

 			</div>
 		</div>
 	</div>
 </nav>
