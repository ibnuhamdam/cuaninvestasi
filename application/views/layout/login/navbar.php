<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?= $title; ?></title>

	<!-- Favicon -->
	<!-- <link rel="icon" href="<?= base_url('assets/img/brand/'); ?>/favicon.png" type="image/png"> -->
	<!-- Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
	<!-- Icons -->
	<link rel="stylesheet" href="<?= base_url('assets/vendor/nucleo/css'); ?>/nucleo.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/nucleo/css/@fortawesome/fontawesome-free/css'); ?>/all.min.css" type="text/css">
	<!-- Argon CSS -->
	<link rel="stylesheet" href="<?= base_url('assets/css'); ?>/argon.css?v=1.2.0" type="text/css">
</head>

<body class="bg-default">
