<div class="main-content" id="panel">
	<div class="container pt-5">
		<div class="row">
			<div class="col-md-12">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?= site_url('Admin/home'); ?>">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Tambah Berita</li>
					</ol>
				</nav>
				<h1 class="display-4 pt-4 pb-2">Tambah Berita CuanInvestasi</h1>

				<div class="tambah-berita px-4 pb-3">
					<form action="<?= site_url('Admin/berita/update'); ?>" method="POST" enctype="multipart/form-data">

						<div class="row">
							<?php foreach ($berita as $news) : ?>
								<div class="col-md-7 col-sm-12">
									<div class="form-group">
										<label for="judul">Judul Berita</label>
										<input type="text" class="form-control" id="judul" value="<?= $news->judul; ?>" name="judul">
									</div>
									<div class="form-group">
										<label for="tags">Tags Berita</label>
										<input type="text" class="form-control" id="tags" value="<?= $news->tags; ?>" name="tags">
									</div>
									<input type="hidden" name="id" value="<?= $news->id; ?>">
									<input type="hidden" name="old_image" value="<?= $news->gambar; ?>">
									<div class="form-group">
										<label for="tags">Gambar</label>
										<div class="custom-file">
											<input type="file" name="image" class="custom-file-input" id="customFileLang" lang="en">
											<label class="custom-file-label" for="customFileLang">Select file</label>
										</div>

									</div>
								</div>
						</div>

						<div class="form-group">
							<label for="exampleFormControlTextarea1">Isi Berita</label>
							<textarea class="form-control" name="isi" id="isi" value="<?= $news->isi; ?>" id="exampleFormControlTextarea1" rows="3"></textarea>
						</div>
					<?php endforeach; ?>

					<button class="btn btn-icon btn-success" type="submit">
						<span class="btn-inner--icon"><i class="ni ni-send"></i></span>
						<span class="btn-inner--text">Simpan</span>
					</button>
					</form>
				</div>

			</div>
		</div>

	</div>
</div>
