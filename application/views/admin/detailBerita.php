<div class="main-content" id="panel">
	<div class="container pt-5 py-5">
		<div class="row">
			<div class="col-md-12">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?= site_url('Admin/home'); ?>">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Detail Berita</li>
					</ol>
				</nav>
				<h1 class="display-4 pt-4 pb-2">Detail Berita</h1>

				<div class="row ">
					<div class="col-md-12">
						<div class="detail">
							<div class="table-responsive">
								<table class="table table-striped table-dark table-bordered">
									<tbody>
										<?php foreach ($berita as $news) : ?>
											<tr>
												<td>Judul Berita</td>
												<td><?= $news->judul; ?></td>
											</tr>
											<tr>
												<td>Isi Berita</td>
												<td><?= $news->isi; ?></td>
											</tr>
											<tr>
												<td>Tags Berita</td>
												<td><?= $news->tags; ?></td>
											</tr>
											<tr>
												<td>Gambar</td>
												<td><img src="<?= base_url("uploads/news/gambar/$news->gambar"); ?>" width="300" class="img-fluid" alt=""></td>
											</tr>
											<tr>
												<td>Dibuat pada</td>
												<td><?= $news->created_at; ?></td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
								<a href="<?= site_url('admin/home/berita'); ?>">
									<button class="btn btn-icon btn-primary" type="button">
										<span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>
										<span class="btn-inner--text">Kembali</span>
									</button>
								</a>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>

	</div>
</div>
