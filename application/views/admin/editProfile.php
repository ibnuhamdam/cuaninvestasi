<div class="main-content" id="panel">
	<div class="container pt-5">
		<div class="row">
			<div class="col-md-12">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?= site_url('Admin/home'); ?>">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Daftar Berita</li>
					</ol>
				</nav>
			</div>

			<div class="col-md-6 offset-md-3">
				<div class="card text-center">
					<div class="col-auto mt-4">
						<div class="icon icon-shape bg-gradient-blue text-white rounded-circle shadow" style="width: 7rem;height: 7rem;">
							<i class="ni ni-single-02 icon-profile" style="font-size: 55px;"></i>
						</div>
					</div>
					<form action="<?= site_url('Admin/home/updateProfile'); ?>" method="POST">
						<div class="card-body">
							<div class="row">
								<div class="col-md-6 offset-md-3">
									<input type="hidden" name="id" value="<?= $this->session->userdata('id'); ?>">
									<div class="form-group">
										<input type="text" value="<?= $akun->nama; ?>" name="nama" class="form-control text-center" id="exampleInputEmail1" aria-describedby="emailHelp">
									</div>
									<div class="form-group">
										<input type="email" value="<?= $akun->email; ?>" name="email" class="form-control text-center" id="exampleInputEmail1" aria-describedby="emailHelp">
									</div>

								</div>
							</div>

						</div>
						<div class="card-footer text-right">
							<a href="<?= site_url('Admin/home/profile'); ?>"><button type="button" class="btn btn-outline-danger">Batal</button></a>
							<button class="btn btn-icon btn-success" type="submit">
								<span class="btn-inner--text">Simpan</span>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
