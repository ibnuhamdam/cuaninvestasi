<div class="main-content" id="panel">
	<div class="container pt-5">
		<div class="row">
			<div class="col-md-12">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?= site_url('Admin/home'); ?>">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Daftar Berita</li>
					</ol>
				</nav>
			</div>

			<div class="col-md-6 offset-md-3">
				<div class="card text-center">
					<div class="col-auto mt-4">
						<div class="icon icon-shape bg-gradient-blue text-white rounded-circle shadow" style="width: 7rem;height: 7rem;">
							<i class="ni ni-single-02 icon-profile" style="font-size: 55px;"></i>
						</div>
					</div>
					<?php if ($this->session->flashdata('update') == 'success') { ?>
						<div class="col-md-10 offset-md-1">
							<div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
								<span class="alert-icon"><i class="ni ni-like-2"></i></span>
								<span class="alert-text"><strong>Data Berhasil Diubah !</strong> </span>
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						</div>
					<?php }; ?>
					<div class="card-body">
						<h1><?= $akun->nama; ?></h1>
						<p class="card-text"><?= $akun->email; ?></p>
					</div>
					<div class="card-footer">
						<a href="<?= site_url('Admin/home/editProfile'); ?>">Edit my profile</a>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
