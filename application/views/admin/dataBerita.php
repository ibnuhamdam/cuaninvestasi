<div class="main-content" id="panel">
	<div class="container pt-5">
		<div class="row">
			<div class="col-md-12">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?= site_url('Admin/home'); ?>">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Daftar Berita</li>
					</ol>
				</nav>

				<h1 class="display-4 pt-4 pb-2">Daftar Berita CuanInvestasi</h1>
				<?php if ($this->session->flashdata('update') == 'success') { ?>
					<div class="alert alert-success alert-dismissible fade show mt-3" role="alert" style="width: 400px;">
						<span class="alert-icon"><i class="ni ni-like-2"></i></span>
						<span class="alert-text"><strong>Data Berhasil Diupdate !</strong> </span>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				<?php }; ?>
				<div class="table-responsive">
					<div>
						<table class="table align-items-center table-dark">
							<thead class="thead-dark">
								<tr>
									<th scope="col" class="sort" data-sort="name">No.</th>
									<th scope="col" class="sort" data-sort="budget" width="200">Judul Berita</th>
									<th scope="col" class="sort" data-sort="status">Tags</th>
									<th scope="col" class="sort" data-sort="completion">Dibuat Pada</th>
									<th scope="col"></th>
									<th scope="col">Aksi</th>
								</tr>
							</thead>
							<tbody class="list">
								<?php foreach ($berita as $news) : ?>
									<tr>
										<td><?= $news->id; ?></td>
										<td><?= $news->judul; ?></td>
										<td><?= $news->tags; ?></td>
										<td><?= $news->created_at; ?></td>
										<td><a href="<?= site_url("admin/home/detailBerita/$news->id"); ?>"><button type="button" class="btn btn-primary btn-sm">Selengkapnya</button></a></td>
										<td>
											<a href="<?= site_url("admin/home/editBerita/$news->id"); ?>" class="text-white">
												<button class="btn btn-icon btn-success btn-sm" type="submit">
													<span class="btn-inner--icon"><i class="ni ni-ruler-pencil"></i></span>
													<span class="btn-inner--text">Ubah</span>
												</button>
											</a>
											<a href="<?= site_url("admin/berita/delete/$news->id"); ?>" class="text-white">
												<button class="btn btn-icon btn-danger btn-sm" type="submit">
													<span class="btn-inner--text">Hapus</span>
												</button>
											</a>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>


			</div>
		</div>

	</div>
</div>
