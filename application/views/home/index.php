<div class="container">
	<section class="headline d-none d-md-block">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="headline-news mt-5 ">
					<div class="row">
						<div class="col-md-6 col-sm-12 pr-0  head-1" style="background:  linear-gradient(to right, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.4)), url('<?= $beritaApi[0]['urlToImage']; ?>');background-size:cover; background-repeat: no-repeat;">
							<a href="<?= $beritaApi[0]['url']; ?>" target="__BLANK">
								<h5><?= $beritaApi[0]['title']; ?></h5>
							</a>
							<hr>
						</div>
						<div class="col-md-6 d-sm-none d-md-block col-sm-12">

							<div class="row">
								<div class="col-md-6 col-sm-12 pr-0 item head-2 text-left" style="background: linear-gradient(to right, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('<?= $beritaApi[1]['urlToImage']; ?>');background-size:cover; background-repeat: no-repeat;">
									<a href="<?= $beritaApi[1]['url']; ?>" target="__BLANK">
										<h5><?= $beritaApi[1]['title']; ?></h5>
									</a>
									<hr>
								</div>
								<div class="col-6 pr-0 item head-2 text-left" style="background: linear-gradient(to right, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('<?= $beritaApi[2]['urlToImage']; ?>');background-size:cover; background-repeat: no-repeat;">
									<a href="<?= $beritaApi[2]['url']; ?>" target="__BLANK">
										<h5><?= $beritaApi[2]['title']; ?></h5>
									</a>
									<hr>
								</div>
							</div>

							<div class="row mt-1">
								<div class="col-6 pr-0 item head-2" style="background: linear-gradient(to right, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('<?= $beritaApi[3]['urlToImage']; ?>');background-size:cover; background-repeat: no-repeat;">
									<a href="<?= $beritaApi[3]['url']; ?>" target="__BLANK">
										<h5><?= $beritaApi[3]['title']; ?></h5>
									</a>
									<hr>
								</div>
								<div class="col-6 pr-0 item head-2" style="background: linear-gradient(to right, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('<?= $beritaApi[4]['urlToImage']; ?>');background-size:cover; background-repeat: no-repeat;">
									<a href="<?= $beritaApi[4]['url']; ?>" target="__BLANK">
										<h5><?= $beritaApi[4]['title']; ?></h5>
									</a>
									<hr>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		<hr>
	</section>

	<section class="olahraga mt-3">

		<div class="row">
			<div class="col-12 mb-2">
				<h5> Olahraga</h5>
				<div class="row mt-4">
					<?php foreach ($beritaOlahraga as $bo) : ?>
						<div class="col-md-6 col-sm-12 content">
							<div class="row">
								<div class="col-md-5 col-6">
									<img src="<?= $bo['urlToImage']; ?>" class="img-fluid" alt="">
								</div>
								<div class="col-md-7 col-6">
									<h5><?= $bo['title']; ?></h5>
									<hr>
									<p class="text-justify"> <?php $isi = explode("[", $bo['content'])[0];
																echo $isi; ?> <a href="<?= $bo['url']; ?>" target="__BLANK">lihat selengkapnya</a></p>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>

	<section class="covid">
		<!-- <hr> -->
		<div class="row">
			<div class="col-md-12 mb-2">
				<h5> Covid-19</h5>
				<div class="row mt-4">
					<?php foreach ($beritaCovid as $bc) : ?>
						<div class="col-md-6 col-sm-12 content">
							<div class="row">
								<div class="col-md-5 col-sm-10">
									<img src="<?= $bc['urlToImage']; ?>" class="img-fluid" alt="">
								</div>
								<div class="col-md-7 col-sm-12">
									<h5><?= $bc['title']; ?></h5>
									<hr>
									<p class="text-justify"> <?php $isi = explode("[", $bc['content'])[0];
																echo $isi; ?> <a href="<?= $bc['url']; ?>" target="__BLANK">lihat selengkapnya</a></p>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>

	<section class="iklan mt-5">
		<div class="row">
			<div class="col-md-12 isi text-center py-auto">
				<h3 class="py-5">Iklan 1140×130px</h3>
			</div>
		</div>
	</section>

	<section class="lain-lain">
		<hr>
		<div class="row">
			<div class="col-md-12 col-sm-12 mb-2">
				<h5> Berita Lainnya</h5>
				<div class="row mt-4">
					<?php foreach ($beritaLainnya as $bl) : ?>
						<div class="col-md-6 col-sm-12 content">
							<div class="row">
								<div class="col-md-6 col-sm-12">
									<img src="<?= $bl['urlToImage']; ?>" class="img-fluid" alt="">
								</div>
								<div class="col-md-6 col-sm-12">
									<p class="text-left text-secondary"><?php $isi = explode("T", $bl['publishedAt'])[0];
																		echo $isi; ?></p>
									<h5> <a href="<?= $bl['url']; ?>" target="__BLANK"> <?= $bl['title']; ?> </a></h5>
									<p class="text-justify"> <?php $isi = explode("[", $bl['content'])[0];
																echo $isi; ?><a href="<?= $bl['url']; ?>" target="__BLANK">lihat selengkapnya</a></p>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-12 justify-content-center text-center">
				<a href="<?= site_url('Pages/allnews'); ?>"><button class="btn btn-lihatlain">Lihat Lainnya</button></a>
			</div>
		</div>
	</section>

	<section class="blog">
		<hr>
		<div class="row">
			<div class="col-md-12">
				<h5>Our Blog</h5>
				<div class="row mb-4 mt-3">
					<?php foreach ($berita as $news) : ?>
						<div class="col-md-4 col-sm-12">
							<div class="row">
								<div class="col-md-6 col-sm-12"><img src="<?= base_url("uploads/news/gambar/$news->gambar"); ?>" class="img-fluid" alt=""></div>
								<div class="col-md-6 col-sm-12">
									<h5> <a href="#"> <?= $news->judul; ?> </a></h5>
									<p class="text-justify"><?= $news->isi; ?></p>
									<p class="text-left text-secondary"><?= $news->penulis; ?></p>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<nav aria-label="Page navigation example">
					<ul class="pagination justify-content-center">
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

</div>
