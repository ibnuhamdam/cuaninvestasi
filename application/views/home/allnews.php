<div class="container mb-3">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<h2>Semua Berita</h2>
			<hr>
			<section>
				<div class="row">
					<?php foreach ($beritaApi as $ba) : ?>
						<div class="col-md-4">
							<img src="<?= $ba['urlToImage']; ?>" class="img-fluid" alt="">
							<h4 style="font-weight: 700;" class="mt-2"><?= $ba['title']; ?></h4>
							<small class="text-secondary"><?php $isi = explode("T", $ba['publishedAt'])[0];
															echo $isi; ?></small>
							<p><?php $isi = explode("[", $ba['content'])[0];
								echo $isi; ?><a href="<?= $ba['url']; ?>" target="__BLANK">lihat selengkapnya</a></p>
						</div>
					<?php endforeach; ?>
				</div>
			</section>
			<section class="iklan my-3">
				<div class="row">
					<div class="col-md-12 isi text-center py-auto">
						<h3 class="py-5">Iklan 1140×130px</h3>
					</div>
				</div>
			</section>
			<section>
				<div class="row">
					<div class="col-md-6">
						<?php foreach ($beritaHealth as $bh) : ?>
							<div class="col-md-12">
								<img src="<?= $bh['urlToImage']; ?>" class="img-fluid" alt="">
								<h4 style="font-weight: 700;" class="mt-2"><?= $bh['title']; ?></h4>
								<small class="text-secondary"><?php $isi = explode("T", $bh['publishedAt'])[0];
																echo $isi; ?></small>
								<p><?php $isi = explode("[", $bh['content'])[0];
									echo $isi; ?><a href="<?= $bh['url']; ?>" target="__BLANK">lihat selengkapnya</a></p>

							</div>
						<?php endforeach; ?>
					</div>
					<div class="col-md-6">
						<div class="row iklan">
							<div class="col-md-12 mb-2 text-center isi" style="height: 230px;">
								<br>
								<h1>Iklan 540x230</h1>
							</div>
						</div>
						<?php foreach ($beritaGeneral as $bg) : ?>
							<div class="row">
								<div class="col-md-6">
									<img src="<?= $bg['urlToImage']; ?>" class="img-fluid" alt="">
								</div>
								<div class="col-md-6">
									<small class="text-secondary"><?php $isi = explode("T", $bg['publishedAt'])[0];
																	echo $isi; ?></small>
									<h4 style="font-weight: 700;" class="mt-2"><?= $bg['title']; ?></h4>
									<p><?php $isi = explode("[", $bg['content'])[0];
										echo $isi; ?><a href="<?= $bg['url']; ?>" target="__BLANK">lihat selengkapnya</a></p>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</section>
			<hr>
		</div>
	</div>
</div>
